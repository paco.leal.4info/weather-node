// token LocationIQ: ac546a994f9078
// token OpenWeatherMap: 29f1012af2f5c651859e99f3d13b83c0

// petition example LocationIQ: https://eu1.locationiq.com/v1/search.php?key=ac546a994f9078&q=Calle%20La%20Ribera%20Murcia%2030010&format=json
// petition example OpenWeatherMap: api.openweathermap.org/data/2.5/weather?lat=38&lon=-1&appid=29f1012af2f5c651859e99f3d13b83c0&units=metric

// response items for LocationIQ: obj0, display_name, lat, lon
// response items for OpenWeatherMap: main>temp

// for xml petitions delete from &json until the end of the URL

const location = require('./location/location');
const weather = require('./weather/weather');
const colors = require('colors');

const argv = require('yargs').options({
    location: {
        alias: 'l',
        desc: 'City or country you want to search the weather for',
        demand: true,
    }
}).argv;

let getInfo = async(loc) => {

    try {
        let coors = await location.getLocation(loc);
        let tmp = await weather.getWeather(coors.lat, coors.lon);

        return `El clima en ${coors.loc} es de ${tmp}ºC`.green;
    } catch (e) {
        return `No se puede determinar el clima de ${loc}`.red;
    }


}

getInfo(argv.location)
    .then(message => console.log(message))
    .catch(e => console.log(e));