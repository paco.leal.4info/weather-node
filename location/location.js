const axios = require('axios');

const getLocation = async(loc) => {

    let encodeUrl = encodeURI(loc);

    let resp = await axios.get(`https://eu1.locationiq.com/v1/search.php?key=ac546a994f9078&q=${encodeUrl}&format=json`)

    if (resp.data.error) {
        throw new Error("Unable to geocode");
    }
    let result = resp.data[0];

    return {
        loc: result.display_name,
        lat: result.lat,
        lon: result.lon
    }
}

module.exports = {
    getLocation
}