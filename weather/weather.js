const axios = require('axios');

const getWeather = async(lat, lon) => {
    lat = Math.round(lat * 100) / 100;
    lon = Math.round(lon * 100) / 100;
    let resp = await axios.get(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=29f1012af2f5c651859e99f3d13b83c0&units=metric`)
    if (resp.data.cod != 200) {
        throw new Error("Error");
    }
    let result = resp.data.main.temp;
    return result;
}

module.exports = {
    getWeather
}